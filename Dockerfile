FROM nginx:1.27.1-alpine

RUN apk update && apk add --no-cache \
    python3 \
    py3-pip

WORKDIR /opt/app
COPY ./python /opt/app/python
RUN pip install -r python/requirements.txt --break-system-packages

COPY ./templates /opt/app/templates
COPY ./entrypoint.sh /opt/app/entrypoint.sh

ENTRYPOINT ["/opt/app/entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]
