#!/bin/sh

# Delete any existing NGINX config
rm -f /etc/nginx/conf.d/*

# Render logging config
python3 /opt/app/python/render.py \
    /opt/app/templates/logging.conf.template \
    /opt/app/config.yaml \
    -o /etc/nginx/conf.d/00-logging.conf

# Render main NGINX config
python3 /opt/app/python/render.py \
    /opt/app/templates/nginx.conf.template \
    /opt/app/config.yaml \
    -o /etc/nginx/conf.d/01-default.conf

# Run original entrypoint from base NGINX image
exec /docker-entrypoint.sh "$@"
