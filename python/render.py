import pathlib
import pkgutil
import importlib
import os
import re
from typing import TYPE_CHECKING, Callable, Dict
import urllib.parse

import click
import jinja2
import yaml

if TYPE_CHECKING:
    import io


# Future improvements: use relative import if possible
def _import_plugins() -> Dict[str, Callable]:
    result = {}
    plugin_dir = pathlib.Path(__file__).parent / "plugins"
    for finder, name, ispkg in pkgutil.iter_modules(path=[str(plugin_dir)]):
        d = os.path.basename(finder.path)
        module = importlib.import_module(f"{d}.{name}")
        funcs = {k: getattr(module, k) for k in dir(module) if callable(getattr(module, k))}
        result.update(funcs)

    return result


def validate_config(config: dict):
    """Validates the provided configuration"""
    default_locations = 0
    for location in config.get("locations", []):
        if not location.get("path"):
            raise ValueError("Location missing required parameter 'path'")
        if not location.get("response_code", location.get("custom")):
            raise ValueError(
                "Must provide either response code (with optional body) or "
                "a custom configuration representing the logic for the entire "
                "location block"
            )
        if location.get("default") is True:
            default_locations += 1
        if default_locations > 1:
            raise ValueError("Cannot have more than one default location")
    logging = config.get("logging")
    if logging:
        fields = logging.get("fields")
        extra_fields = logging.get("extra_fields")
        custom = logging.get("custom_format")
        num_included = sum(
            f is not None for f in (fields, extra_fields, custom)
        )
        if num_included > 1:
            raise ValueError(
                "Logging configuration must include *only* one of 'fields', "
                "'extra_fields', or 'custom_format' field"
            )
        if num_included == 0:
            raise ValueError(
                "Logging configuration must include one of 'fields', "
                "'extra_fields', or 'custom_format' field"
            )


def location_body(location: dict):
    """
    Converts a provided dict representing an NGINX location block to a string.
    The dict should have been previously validated.
    """
    has_body = False
    location_block = None
    if location.get("custom"):
        location_block = location["custom"].strip()

        # Add trailing semicolon, if needed
        if not location_block.endswith(";"):
            location_block += ";"
    elif location.get("body"):
        location_block = location["body"].strip()
        has_body = True
        #location_block = f"return {location['response_code']}"
    else:
        location_block = f"return {location['response_code']};"

    # Render location block as a template
    env = jinja2.Environment(
        loader=jinja2.BaseLoader,
        lstrip_blocks=True,
    )
    template = env.from_string(location_block)
    plugins = _import_plugins()
    template.globals.update(plugins)
    location_block = template.render()

    # For the case where a body was provided directly, escape any single
    # quotes, since we use those to surround the body. Also replace newlines
    # with escaped newline characters
    if has_body:
        location_block = re.sub(r"([^\\]{0,1})'", r"\1\\'", location_block)

        # Escape all newlines so they get rendered in the nginx config
        # as '\n' and the return value gets indented correctly.
        location_block = location_block.replace("\n", "\\n")
        location_block = (
            f"return {location['response_code']} '{location_block}';"
        )

    return location_block


@click.command()
@click.argument("template-file", type=click.Path(exists=True))
@click.argument("config-file", type=click.File())
@click.option(
    "-o",
    "--output-file",
    type=click.File(mode="w", lazy=True),
    default="-",
    show_default=True,
    help="File to write the rendered template to.",
)
def render(
    template_file: str,
    config_file: "io.BufferedReader",
    output_file: click.utils.LazyFile,
):
    """
    Renders a provided Jinja2 template using a provided YAML config file.
    """

    # Import any custom plugins
    plugins = _import_plugins()

    # Set up jinja2 environment.
    p = pathlib.Path(template_file)
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader(str(p.parent.absolute())),
        lstrip_blocks=True,
    )

    # Load template.
    template = env.get_template(p.name)
    template.globals.update(
        {
            "location_body": location_body,
            "urljoin": urllib.parse.urljoin,
        }
    )
    template.globals.update(plugins)

    # Read in config file.
    config_data = config_file.read()
    config = yaml.safe_load(config_data)
    validate_config(config)

    # Update config with default location if one exists
    default_location = None
    for loc in config.get("locations", []):
        if loc.get("default") is True:
            default_location = loc
            break
    if default_location:
        config["default_location"] = default_location

    # Render template with data from config file.
    rendered_template = template.render(**config)

    # Write to file.
    output_file.write(rendered_template)


if __name__ == "__main__":
    render()
