#! /bin/bash
# Usage: ./run.sh
set -uo pipefail

#########
# Setup #
#########
# Check if prerequisites are installed
PREREQS=( yq python3 )
for CMD in "${PREREQS[@]}"; do
    if ! command -v "${CMD}" &> /dev/null; then
        echo "${CMD} must be installed to run the tests. Exiting..."
        exit 1
    fi
done

# Get directory where this script is.
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Paths to templates.
NGINX_TEMPLATE="${SCRIPT_DIR}/../templates/nginx.conf.template"
LOGGING_TEMPLATE="${SCRIPT_DIR}/../templates/logging.conf.template"

# Path to YAML file defining all tests.
TEST_FILE="${SCRIPT_DIR}/tests.yaml"

# Load tests into array. Each element is a YAML string.
readarray TESTS < <(yq e -o=j -I=0 '.[]' "${TEST_FILE}")

####################
# Helper functions #
####################
render() {
    # Usage: render <template-file> <config-file>
    python3 "${SCRIPT_DIR}/../python/render.py" "${1}" "${2}"
}

run_test() {
    # Usage: run_test <test>
    local TEST_CONFIG="$(get_test_field "${1}" "config")"
    local TEMPLATE_FILE="${NGINX_TEMPLATE}"
    if [[ "$(get_test_field "${1}" "config.logging")" != "null" ]]; then
        TEMPLATE_FILE="${LOGGING_TEMPLATE}"
    fi
    render "${TEMPLATE_FILE}" <(echo "${TEST_CONFIG}")
}

compare_results() {
    # Usage: compare_results <test> <test-result>
    diff -u <(echo "${2}") <(get_test_field "${1}" "result.string") | tail -n +4
}

should_fail() {
    # Usage: should_fail <test>
    # Returns "false" for tests that shouldn't fail
    # Returns "true" for tests that should fail
    local SHOULD_FAIL="$(get_test_field "${1}" "result.fail")"
    local EXPECTED_RESULT="true"
    if [[ "${SHOULD_FAIL}" == "null" ]] || [[ "${SHOULD_FAIL}" == "false" ]]; then
        # Not expected to fail
        EXPECTED_RESULT="false"
    fi
    echo "${EXPECTED_RESULT}"
}

check_fail() {
    # Usage: check_fail <test> <test-run-exit-code>
    local SHOULD_FAIL="$(should_fail "${TEST}")"
    if [[ "${SHOULD_FAIL}" == "true" ]] && [[ "${2}" -gt 0 ]]; then
        # Expected to fail and did fail
        return 0
    elif [[ "${SHOULD_FAIL}" == "false" ]] && [[ "${2}" -eq 0 ]]; then
        return 0
    fi
    return 1
}

get_test_field() {
    # Usage: get_test_field <test> <field>
    echo "${1}" | yq e '.'${2}
}

echoindent() {
    # Usage: echoindent <string>
    echo "${1}" | sed 's/^/    /'
}

########
# Main #
########
# Iterate over tests. Exit immediately if one fails.
n=1
for TEST in "${TESTS[@]}"; do
    echo -n "Running test ${n}: '$(get_test_field "${TEST}" name)'. Result: "

    # Run test
    TEST_RESULT=$(run_test "${TEST}" 2>&1)
    RENDER_RESULT=$?

    # Check if supposed to fail.
    if ! check_fail "${TEST}" "${RENDER_RESULT}"; then
        echo "failed."
        echoindent "Expected rendering to fail: $(should_fail "${TEST}")." 
        echoindent "Rendering exit code: ${RENDER_RESULT}"
        echoindent "${TEST_RESULT}"
        exit 1
    fi

    # If not expected to fail, compare results.
    if [[ "$(should_fail "${TEST}")" != "true" ]]; then
        TEST_DIFF=$(compare_results "${TEST}" "${TEST_RESULT}")
        if [[ "$?" -ne 0 ]]; then
            echo "failed."
            echoindent "Expected configuration (-) did not match rendered template (+)."
            echoindent "${TEST_DIFF}"
            exit 1
        fi
    else
        # If expected to fail and an error string is provided, make sure
        # it matches.
        ERROR_STRING=$(get_test_field "${TEST}" "result.error")
        if [[ "${ERROR_STRING}" != "null" ]]; then
            # Get last line of Python traceback
            LAST_LINE="$(echo "${TEST_RESULT}" | tail -n 1)"
            if ! [[ "${LAST_LINE}" =~ ${ERROR_STRING} ]]; then
                echo "failed."
                echoindent "Observed error message (-) did not match expected regex (+):"
                echoindent "$(diff -u <(echo "${LAST_LINE}") <(echo "${ERROR_STRING}") | tail -n +4)"
                exit 1
            fi
        fi
    fi

    echo "success."
    n=$((n+1))
done

echo "All tests passed."
