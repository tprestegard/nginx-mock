# nginx-mock
nginx-mock provides a Docker image that runs [NGINX](https://www.nginx.com/).
It takes in a provided YAML configuration file and uses it to render a template representing an NGINX server configuration.
Then, it runs NGINX using the base configuration and the additional server configuration.
It is primarily useful as a simple way to mock responses for testing other applications.


## Example configuration
An example configuration is shown here.
Additional examples can be seen in the [examples](./examples/) directory or the [tests file](./tests/tests.yaml).
```
locations:
  - path: /test1
    path_modifier: "~"
    response_code: 404
  - path: /test2
    response_code: 200
    body: "{\"key\": \"value\", \"key2\": 12}"
  - path: /test3
    custom: |-
      if ($http_authorization = "Bearer: admin") {
          return 200 'Success';
      }
      return 401 'Invalid authorization';
  - path: /default
    path_modifier: "="
    response_code: 200
    body: "default route"
    default: true
logging:
  extra_fields:
    - $http_x_forwarded_for
    - $request_body
    - "\"$http_authorization\""
    - "$http_x_api_key"
```

### Locations
Location directives can be configured under the `locations` key.
Each location must contain a `path` entry as well as a `response_code` or a `custom` field.
If `custom` is used, it should contain the complete logic for the location directive it represents.
If a `response_code` is provided, an optional `body` field can be provided, as well.
An optional `path_modifier` can also be provided for each location.
Make sure to quote the path modifier, as `~` unescaped represents NULL in YAML syntax.

If the `default` field is set to `true`, the configuration will be rendered to route all requests that do not match another location directive to this route.
A "default" route is not required, but only a single one is allowed.

### Server name
By default, the server name is configured as `localhost`.
This can be overridden with a top-level `server_name` field in the config file.

### Logging
The logging format can be modified or overridden.
Provide a `custom_format` field to fully specify logging options and fields.
Example:
```
logging:
  custom_format: |-
    escape=test  '$http_authorization "$request_body"'
                 '"$http_x_api_key"';
```

Provide a `fields` field to specify the logging fields to use.
Provide an `extra_fields` field to add fields to the default logging fields.

## Usage
Run container with example config:
```
docker run --rm -p 80:80 -v /path/to/config.yaml:/opt/app/config.yaml registry.gitlab.com/tprestegard/nginx-mock:latest
```

### Examples
Run container with example config:
```
docker run --rm -p 80:80 -v ${PWD}/examples/config.yaml:/opt/app/config.yaml registry.gitlab.com/tprestegard/nginx-mock:latest
```

Example requests:
```
$ curl http://127.0.0.1:80/test
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.25.2</center>
</body>
</html>

$ curl http://127.0.0.1:80/test2
{"key": "value", "key2": 12}

$ curl http://127.0.0.1:80/test3
default route

$ curl -H 'X-Api-Key: my-key' http://127.0.0.1:80/index.php/fake-route
Good API key

$ curl -H 'X-Api-Key: other-key' http://127.0.0.1:80/index.php/fake-route
Invalid API key
```

## Customization
There are a variety of mechanisms for customizing the NGINX server configuration.
Jinja2 templates are used for templating the primary NGINX configuration and the logging configuration.
These are rendered by the entrypoint script and then imported into the base configuration.

### Override base configuration
Users can provide their own NGINX base configuration by adding a volume mount to this command:
```
-v /path/to/custom/nginx.conf:/etc/nginx/nginx.conf:ro
```

As long as the configuration has an `include /etc/nginx/conf.d/*.conf;` directive, the templated configuration should still be included.

### Override templates
The [provided templates](./templates/) are used by default.
To override them, use a volume mount:
```
docker run --rm -p 80:80 \
    -v /path/to/conf.template:/opt/app/templates/nginx.conf.template \
    -v /path/to/logging.conf.template:/opt/app/templates/logging.conf.template \
    -v /path/to/config.yaml:/opt/app/config.yaml \
    registry.gitlab.com/tprestegard/nginx-mock:latest
```

### Plugins
Python functions can be defined for executing custom logic in Jinja2 templates.
These functions can be used in custom templates or in location entries in a config file (specifically, in `custom`, `body`, or `response_code` fields).
In order to do this, create a Python file and volume mount it into the corresponding directory in the container.

Python file:
```
# plugin.py
def reverse(s: str) -> str:
    return s[::-1]
```

Custom template:
```
# custom-template.conf
server {
    {{ reverse(';08 netsil') }}
}
```

Custom config:
```
# config.yaml
locations:
  - path: /default
    custom: |-
      {{ reverse("; '!olleH' 002 nruter") }}
```

Command:
```
docker run --rm -p 80:80
    -v $(pwd)/custom-template.conf:/opt/app/templates/nginx.conf.template \
    -v $(pwd)/plugin.py:/opt/app/python/plugins/plugin.py \
    -v $(pwd)/config.yaml:/opt/app/config.yaml \
    registry.gitlab.com/tprestegard/nginx-mock:latest
```

## Tests
Tests are defined in `tests/tests.yaml` and executed by running `./tests/run.sh`.

The test definition file includes an array of test objects.
Each test should contain a `config` section which represents a configuration used for rendering either the NGINX template or the logging template.
Each test should also contain a `result` section describing the expected results of the tests

See the existing tests for more details.

### Tests expected to succeed
The `result` section may contain:
* `string`: contains the full rendered template that is expected as a result.
* `fail` (optional): specifying whether the test is expected to fail. Defaults to `false` if not set; should always be `false` for this type of test.

### Tests expected to fail
The `result` section may contain:
* `fail`: should always be set to `true` for this type of test; this defines the test as being expected to fail.
* `error` (optional): if set, this should be a Bash regex which is expected to match the last line of the Python error traceback from the rendering script.

